-- Your SQL goes here
CREATE TABLE IF NOT EXISTS public.documents
(
    document_id uuid DEFAULT uuid_generate_v4(),
    title character varying(200) NOT NULL,
    description text,
    url text NOT NULL,
    counter_downl integer NOT NULL DEFAULT 0,
    author uuid NOT NULL,
    PRIMARY KEY (document_id)
);

ALTER TABLE IF EXISTS public.documents
    ADD FOREIGN KEY (author)
    REFERENCES public.users (user_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;
