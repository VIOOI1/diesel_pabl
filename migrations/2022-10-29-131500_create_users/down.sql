-- This file should undo anything in `up.sql`

DROP TABLE IF EXISTS public.users;
DROP TABLE IF EXISTS public."Role";

DROP EXTENSION IF EXISTS "uuid-ossp";
