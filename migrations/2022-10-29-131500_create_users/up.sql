-- Your SQL goes here
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS public.users
(
    user_id uuid DEFAULT uuid_generate_v4(),
    login character varying(50) NOT NULL,
    email character varying(50) NOT NULL,
    password character varying(200) NOT NULL,
    "firstName" character varying(50) NOT NULL,
    "lastName" character varying(50) NOT NULL,
    "birthData" date,
    role integer,
    PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS public."Role"
(
    role_id serial NOT NULL,
    role character varying(50) NOT NULL,
    PRIMARY KEY (role_id)
);

ALTER TABLE IF EXISTS public.users
    ADD CONSTRAINT role_link FOREIGN KEY (role)
    REFERENCES public."Role" (role_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;


