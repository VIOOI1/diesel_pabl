// Generated by diesel_ext

#![allow(unused)]
#![allow(clippy::all)]

use bcrypt;
use chrono::NaiveDate;
use diesel::prelude::*;
use diesel::{Identifiable, Queryable};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Queryable, Debug)]
pub struct Role {
    pub role_id: i32,
    pub role: String,
}

#[derive(PartialEq, Serialize, Deserialize, Queryable, Debug)]
pub struct Document {
    pub document_id: Uuid,
    pub title: String,
    pub description: Option<String>,
    pub url: String,
    pub counter_downl: i32,
    pub author: Uuid,
}

#[derive(PartialEq, Serialize, Deserialize, Queryable, Debug)]
pub struct User {
    pub user_id: Uuid,
    pub login: String,
    pub email: String,
    pub password: String,
    #[serde(rename = "firstName")]
    pub first_name: String,
    #[serde(rename = "lastName")]
    pub last_name: String,
    #[serde(rename = "birthData")]
    pub birth_data: Option<NaiveDate>,
    pub role: Option<i32>,
}

impl User {
    // add code here
    pub fn new(
        login: String,
        email: String,
        password: String,
        first_name: String,
        last_name: String,
        role: i32,
    ) -> User {
        User {
            user_id: Uuid::new_v4(),
            login,
            email,
            password,
            first_name,
            last_name,
            birth_data: None,
            role: Some(role),
        }
    }
}
