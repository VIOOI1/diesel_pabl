// @generated automatically by Diesel CLI.

diesel::table! {
    Role (role_id) {
        role_id -> Int4,
        role -> Varchar,
    }
}

diesel::table! {
    documents (document_id) {
        document_id -> Uuid,
        title -> Varchar,
        description -> Nullable<Text>,
        url -> Text,
        counter_downl -> Int4,
        author -> Uuid,
    }
}

diesel::table! {
    users (user_id) {
        user_id -> Uuid,
        login -> Varchar,
        email -> Varchar,
        password -> Varchar,
        #[sql_name = "firstName"]
        first_name -> Varchar,
        #[sql_name = "lastName"]
        last_name -> Varchar,
        #[sql_name = "birthData"]
        birthData -> Nullable<Date>,
        role -> Nullable<Int4>,
    }
}

diesel::joinable!(documents -> users (author));
diesel::joinable!(users -> Role (role));

diesel::allow_tables_to_appear_in_same_query!(Role, documents, users,);
