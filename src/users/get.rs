use crate::{establish_connection, models::User};
use diesel::prelude::*;
use diesel::RunQueryDsl;
use uuid;

/// Функция для запроса всех пользователей из базы данных
pub fn get_all_users() -> Result<Vec<User>, diesel::result::Error> {
    use crate::schema::users::dsl::*;
    let conn = &mut establish_connection();

    let result = users.get_results::<User>(conn);
    // println!("{:#?}", &result.as_ref());
    result
}

#[test]
#[ignore = "test_get_all"]
fn test_get_all() {
    let r = get_all_users().unwrap();
    println!("{:#?}", r);
}

/// Функция для получения пользователя по его uuid
pub fn get_user_by_id(id: String) -> Result<User, diesel::result::Error> {
    use crate::schema::users::dsl::*;
    let conn = &mut establish_connection();

    let id = uuid::Uuid::parse_str(&id).unwrap();

    let result = users.filter(user_id.eq(id)).first::<User>(conn);
    result
}

/// Функция для получения пользователя по его логину
pub fn get_user_by_login(user_login: String) -> Result<User, diesel::result::Error> {
    use crate::schema::users::dsl::*;
    let conn = &mut establish_connection();

    let result = users.filter(login.eq(user_login)).first::<User>(conn);
    result
}

pub fn is_empty_email(vemail: &String) -> bool {
    use crate::schema::users::dsl::*;
    let conn = &mut establish_connection();

    let result = users.filter(email.eq(vemail)).first::<User>(conn);

    match result {
        Ok(_) => true,
        Err(_) => false,
    }
}
