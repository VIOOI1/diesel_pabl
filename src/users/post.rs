use bcrypt;
use bcrypt::DEFAULT_COST;
use diesel::prelude::*;
use diesel::Insertable;
use serde::{Deserialize, Serialize};

use crate::models::User;
use crate::schema::users;

#[derive(Deserialize, Serialize, Insertable, Debug)]
#[diesel(table_name = users)]
pub(self) struct InsertUser {
    login: String,
    email: String,
    password: String,
    first_name: String,
    last_name: String,
}

impl InsertUser {
    fn new(
        login: String,
        email: String,
        password: String,
        first_name: String,
        last_name: String,
    ) -> InsertUser {
        InsertUser {
            login,
            email,
            password,
            first_name,
            last_name,
        }
    }
    fn encryption(self) -> InsertUser {
        let pass = self.password;
        InsertUser {
            password: bcrypt::hash(pass, DEFAULT_COST).unwrap(),
            ..self
        }
    }
}

pub fn create_user(
    login: String,
    email: String,
    password: String,
    first_name: String,
    last_name: String,
) -> Result<User, diesel::result::Error> {
    let conn = &mut crate::establish_connection();

    let new_user = InsertUser::new(login, email, password, first_name, last_name).encryption();

    let result = diesel::insert_into(users::table)
        .values(new_user)
        .get_result::<User>(conn);

    result
}

#[test]
fn test_create_user() {
    let r = create_user(
        "test1".to_string(),
        "test@example".to_string(),
        "1234".to_string(),
        "test1".to_string(),
        "test1".to_string(),
    );

    println!("{:#?}", r.unwrap())
}
